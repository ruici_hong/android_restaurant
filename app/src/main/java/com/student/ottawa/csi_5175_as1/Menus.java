package com.student.ottawa.csi_5175_as1;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Menus  extends ActionBarActivity implements View.OnClickListener {

    private TextView tvTitle;
    private GalleryView gallery;
    private ImageAdapter adapter;
    private CheckBox cb_01;
    private Map orders = new HashMap<String,Boolean>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menus);
        initRes();
    }

    private void initRes(){
        View  v =  findViewById(R.id.bottom_1);
        Utils utils = new Utils(this);
        utils.setBottom(v);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        gallery = (GalleryView) findViewById(R.id.mygallery);
        cb_01 = (CheckBox) findViewById(R.id.cb_01);
        cb_01.setOnClickListener(this);
        adapter = new ImageAdapter(this);
        adapter.createReflectedImages();
        gallery.setAdapter(adapter);
        int length = adapter.titles.length;
        for(int i=0; i<length; i++){
         orders.put(adapter.titles[i],false);
       }


        gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvTitle.setText(adapter.titles[position]);
                cb_01.setChecked((Boolean)orders.get(adapter.titles[position]));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)      {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menus, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_submit){
        int orders_size = 0;
            Iterator iter = orders.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Boolean val = (Boolean)entry.getValue();
                if(val)
                orders_size++;
            }
        Intent intent = new Intent(Menus.this,MainActivity.class);
        intent.putExtra("orderNumber",orders_size+"");
        startActivity(intent);
            System.out.println("orders_size "+orders_size);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.cb_01){
        int position = gallery.getSelectedItemPosition();
        orders.put(adapter.titles[position],cb_01.isChecked());
        }
    }
}