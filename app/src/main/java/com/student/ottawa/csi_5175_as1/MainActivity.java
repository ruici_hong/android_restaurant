package com.student.ottawa.csi_5175_as1;


import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;


import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private TextView tv_orders;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        Drawable drawable = getResources().getDrawable(R.drawable.food_icon);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setIcon(drawable);
        init();
    }

    public void init(){
        tv_orders = (TextView)findViewById(R.id.tv_orders);
        View  v =  findViewById(R.id.bottom_1);
        Utils utils = new Utils(this);
        utils.setBottom(v);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent=getIntent();
        String order_numeber =  intent.getStringExtra("orderNumber");
        if(order_numeber==null)
        tv_orders.setText("You have total number of orders: 0");
        else
        tv_orders.setText("You have total number of orders: "+order_numeber);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)      {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent = null;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.login){
            intent = new Intent(MainActivity.this,Login.class);
        }
        else if(id == R.id.contacts){
            intent = new Intent(MainActivity.this,Contacts.class);
        }
        else if (id == R.id.menus){
            intent = new Intent(MainActivity.this,Menus.class);
        }

        if(intent!=null)
        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }
}
