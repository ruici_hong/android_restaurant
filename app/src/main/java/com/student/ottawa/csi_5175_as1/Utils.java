package com.student.ottawa.csi_5175_as1;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by youjiahan on 15-02-12.
 */
public class Utils implements View.OnClickListener {

    private Context context;

    public Utils(Context context){
        this.context = context;
    }

    public void setBottom(View v){
         ImageView img_facebook;
         ImageView img_twitter;
         ImageView img_linkedin;
        img_facebook = (ImageView)v.findViewById(R.id.img_facebook);
        img_twitter = (ImageView)v.findViewById(R.id.img_twitter);
        img_linkedin = (ImageView)v.findViewById(R.id.img_linkedin);
        img_facebook.setOnClickListener(this);
        img_twitter.setOnClickListener(this);
        img_linkedin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if(v.getId()==R.id.img_facebook){
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://facebook.com"));

        }else if(v.getId()==R.id.img_twitter){
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://twitter.com"));
        }else if(v.getId()==R.id.img_linkedin){
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://linkedin.com"));
        }
        intent.addCategory(Intent. CATEGORY_BROWSABLE);
        intent.addCategory(Intent. CATEGORY_DEFAULT);
        context.startActivity(intent);
    }
}
