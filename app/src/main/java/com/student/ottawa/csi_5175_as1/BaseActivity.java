package com.student.ottawa.csi_5175_as1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class BaseActivity extends ActionBarActivity implements View.OnClickListener {

    private ImageView img_facebook;
    private ImageView img_twitter;
    private ImageView img_linkedin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        img_facebook = (ImageView)findViewById(R.id.img_facebook);
        img_twitter = (ImageView)findViewById(R.id.img_twitter);
        img_linkedin = (ImageView)findViewById(R.id.img_linkedin);
        img_facebook.setOnClickListener(this);
        img_twitter.setOnClickListener(this);
        img_linkedin.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if(v.getId()==R.id.img_facebook){
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://facebook.com"));

        }else if(v.getId()==R.id.img_twitter){
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://twitter.com"));
        }else if(v.getId()==R.id.img_linkedin){
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://linkedin.com"));
        }
        intent.addCategory(Intent. CATEGORY_BROWSABLE);
        intent.addCategory(Intent. CATEGORY_DEFAULT);
        startActivity(intent);
    }
}
