package com.student.ottawa.csi_5175_as1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class Contacts extends ActionBarActivity {

    private ImageView img_ruicihong;
    private ImageView img_zhaozhongyuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        View v =  findViewById(R.id.bottom_1);
        Utils utils = new Utils(this);
        utils.setBottom(v);

        img_ruicihong = (ImageView)findViewById(R.id.img_ruicihong);
        img_zhaozhongyuan = (ImageView)findViewById(R.id.img_zhaozhongyuan);
        img_ruicihong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/profile/public-profile-settings?trk=prof-edit-edit-public_profile"));
                intent.addCategory(Intent. CATEGORY_BROWSABLE);
                intent.addCategory(Intent. CATEGORY_DEFAULT);
                startActivity(intent);
            }
        });

        img_zhaozhongyuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("ca.linkedin.com/pub/zhongyuan-zhao/8a/309/51b/en"));
                intent.addCategory(Intent. CATEGORY_BROWSABLE);
                intent.addCategory(Intent. CATEGORY_DEFAULT);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
